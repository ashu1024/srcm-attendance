package org.heartfulness.attendance.shared;

import java.util.ArrayList;
import java.util.List;

public class SatsanghTags {
	private String id;
	private String centerName;
	private String centerType;
	private String satsanghDay;
	private List<String> tags;

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getCenterType() {
		return centerType;
	}

	public void setCenterType(String centerType) {
		this.centerType = centerType;
	}

	public String getSatsanghDay() {
		return satsanghDay;
	}

	public void setSatsanghDay(String satsanghDay) {
		this.satsanghDay = satsanghDay;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	
	public void setTags(String csv){
		String[] splits = csv.split("\\|");
		
		List<String> tags = new ArrayList<String>();
		
		for (String split : splits) {
			tags.add(split);
		}
		
		setTags(tags);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
