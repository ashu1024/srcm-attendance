package org.heartfulness.attendance.shared;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class ChatMessage implements Serializable {
	private Integer sNo;

	private Date messageDate;
	private String contact;
	private String messageContent;
	private String anonymousReason;
	private String comment;

	public Date getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(Date messageDate) {
		this.messageDate = messageDate;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}
	
	public Integer getsNo() {
		return sNo;
	}

	public void setsNo(Integer sNo) {
		this.sNo = sNo;
	}

	public String getAnonymousReason() {
		return anonymousReason;
	}

	public void setAnonymousReason(String anonymousReason) {
		this.anonymousReason = anonymousReason;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
