package org.heartfulness.attendance.server;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.heartfulness.attendance.shared.ChatMessage;
import org.heartfulness.attendance.shared.SatsanghTags;

import com.google.gwt.dev.util.collect.HashMap;
import com.google.gwt.dev.util.collect.HashSet;

public class ExtractCurrentWeekChatFile {
	
	private static String allMessageFileName = "AllMessages.html";

	public static void main(String[] args){
		String dir = "/Users/havarma/harish/srcm/heartfulness/satsangh_attendance/2016_Oct_16/";
		WhatsappChat chat = new WhatsappChat(new File(dir + "WhatsApp Chat with GroupMeditationAttendance.txt"));
		List<ChatMessage> weekMessages = new ArrayList<>();
		Date weekStartDate = null;
		try {
			weekStartDate = new SimpleDateFormat("dd-MM-yyyy").parse("10-10-2016");
		} catch (ParseException e1) {
			e1.printStackTrace();
		}
		
		HashSet<ChatMessage> unKnownChatMessage = new HashSet<>();
		
		for (ChatMessage msg : chat.getChatMessages()) {
			if(msg.getMessageDate().compareTo(weekStartDate) >= 0){
				weekMessages.add(msg);
				unKnownChatMessage.add(msg);
			}
		}
		
		List<String> lines = FileUtils.readLines("/Users/havarma/harish/srcm/heartfulness/satsangh_attendance/SatsanghTags.csv");
		List<SatsanghTags> tags = new ArrayList<>();
		HashMap<String, SatsanghTags> tagsMap = new HashMap<>();
		int lineNo = 1;
		
		for (String line : lines) {
			String[] lineSplits = line.split(",");
			
			SatsanghTags tag = new SatsanghTags();
			tag.setId(lineSplits[0]);
			tag.setCenterName(lineSplits[1]);
			tag.setCenterType(lineSplits[2]);
			tag.setSatsanghDay(lineSplits[3]);
			tag.setTags(lineSplits[4]);
			tags.add(tag);
			
			tagsMap.put(tag.getCenterName(), tag);
		}
		
		String htmlDirPath = dir + "output/";
		File htmlDir = new File(htmlDirPath);
		
		if(!htmlDir.exists()){
			htmlDir.mkdirs();
		}
		
		HashMap<Integer, HashMap<String, List<ChatMessage>>> tagMatchingMessagesMap = new HashMap<>();
		int index = 0;
		
		for (SatsanghTags tag : tags) {
			HashMap<String, List<ChatMessage>> tagMatchingMap = new HashMap<>();
			tagMatchingMessagesMap.put(index, tagMatchingMap);
			
			for (String ta : tag.getTags()) {
				List<ChatMessage> messages = new ArrayList<>();
				tagMatchingMap.put(ta, messages);
				
				for (ChatMessage msg : weekMessages) {
					//direct match found - no need to check for typo correction
					if (msg.getMessageContent().toLowerCase().contains(ta.toLowerCase())){
						messages.add(msg);
						
						if(unKnownChatMessage.contains(msg)){
							unKnownChatMessage.remove(msg);
						}
					}else{
						//checking for typo correction here
						for(String word : msg.getMessageContent().split(" ")){
							Integer difference = editDistance(word.toLowerCase(), ta.toLowerCase());	
							//if the word is less than 4 characters, we'll tolerate only 1 typo, otherwise upto 2		
							if((ta.length()<=3&&difference<=1)||(ta.length>3&&difference<=2)){
								messages.add(msg);
								if(unKnownChatMessage.contains(msg)){
									unKnownChatMessage.remove(msg);
								}
							}
						}

					}
				}
			}
			
			index++;
		}
		
		String allMessagesPath = writeAllMessages(htmlDirPath, weekMessages);
		String unIdentifiedMessagesPath = writeNonIdentifiedMessages(htmlDirPath, unKnownChatMessage, weekMessages);
		
		index = 0;
		HashMap<SatsanghTags, String> satsangFilePath = new HashMap<>();
		
		for (SatsanghTags tag : tags) {
			String filePath = writeSatsangCenterMessage(htmlDirPath, index, tag, tagMatchingMessagesMap.get(index), allMessagesPath, unIdentifiedMessagesPath, weekMessages);	
			satsangFilePath.put(tag, filePath);
			index++;
		}
		
		String filePath = writeSatsanghCenters(htmlDirPath, tags, satsangFilePath, allMessagesPath, unIdentifiedMessagesPath);
		
		System.out.println(filePath);
	}

	private static Integer editDistance(String x, String y){
		char [] a = x.toLowerCase().toCharArray();
		char [] b = y.toLowerCase().toCharArray();
		Integer m = x.length(), n = y.length();
		Integer [][] dp = new Integer [m+1][n+1];
		for(Integer i=0 ; i<=m ; i++){
			for(Integer j=0; j<=n ; j++){
				if(i>0||j>0){
					if(i>0&&j>0&&a[i-1]==b[j-1]){
						dp[i][j]=dp[i-1][j-1];
					}else{
						//taking the best scenario out of removing one from x, removing 1 from y or removing 1 from each
						dp[i][j]= Math.min( (i>0?dp[i-1][j]:10000), (j>0?dp[i][j-1]:10000)) +1;
						dp[i][j] = Math.min( dp[i][j] , i>0&&j>0?dp[i-1][j-1]+1: 10000);
					}
				}else{
					dp[i][j]=0;
				}
			}
		}
		System.out.println("edit distance between " + x + " and " + y + " is " + dp[m][n].toString());
		return dp[m][n];
	}
	
	private static String writeAllMessages(String htmlDirPath, List<ChatMessage> weekMessages){
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><style>li { margin: 5px;}</style></head><body><ul>");
		
		for (ChatMessage msg : weekMessages) {
			getLiOfMessages(msg, sb);
		}
		
		sb.append("</ul></body></html>");
		
		String fileName = allMessageFileName;
		String filePath = htmlDirPath + fileName;
		FileUtils.writeToFile(filePath, sb.toString());
		return fileName;
	}
	
	private static void getLiOfMessages(ChatMessage msg, StringBuilder sb){
	 sb.append("<li>").append(msg.getMessageDate()).append(" : ").append(msg.getMessageContent()).append("</li>");;
	}
	
	private static String writeSatsangCenterMessage(String htmlDirPath, int index, SatsanghTags satsangh, HashMap<String, List<ChatMessage>> tagMatchingMap, String allMessagesFilePath, String unIdentifiedFielPath, List<ChatMessage> allChatMessages){
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><style>li { margin: 5px;} </style></head><body><h1>").append(satsangh.getCenterName()).append(" - ").append(satsangh.getCenterType()).append(" - ").append(satsangh.getSatsanghDay()).append("</h1><ul>");
		
		for (String tag : satsangh.getTags()) {
			if(tagMatchingMap.get(tag) == null || tagMatchingMap.get(tag).size() == 0){
				sb.append("<li style=\"color:red\").append(b)\">").append(tag).append("</li>");
			} else{
				sb.append("<li style=\"color:blue\").append(b)\">").append(tag).append("</li>");
			}
		}
		
		sb.append("</ul>");
		
		HashSet<ChatMessage> chatMessagesReferred = new HashSet<>();
		
		for (String tag : satsangh.getTags()) {
			if(tagMatchingMap.get(tag) != null && tagMatchingMap.get(tag).size() > 0){
				sb.append("<h3>").append(tag).append("</h3>");
				sb.append("<ul>");
				
				for (ChatMessage msg : tagMatchingMap.get(tag)) {
					chatMessagesReferred.add(msg);
					
					sb.append("<li>").append(msg.getMessageDate()).append(" : ").append(msg.getMessageContent().toLowerCase()
							.replaceAll(tag.toLowerCase(), "<strong>" + tag.toLowerCase() + "</strong>")).append("</li>");
				}
				
				sb.append("</ul>");
			}
		}
		
		sb.append("<h3>").append("Remaining messages").append("</h3>");
		sb.append("<ul>");
		
		for (ChatMessage chatMessage : allChatMessages) {
			if(!chatMessagesReferred.contains(chatMessage)){
				getLiOfMessages(chatMessage, sb);
			}
		}
		
		sb.append("<ul>");
		
		sb.append("</body></html>");
		
		String fileName = index + ".html";;
		String filePath = htmlDirPath + fileName;
		FileUtils.writeToFile(filePath, sb.toString());
		return fileName;
	}
	
	private static String writeNonIdentifiedMessages(String htmlDirPath, HashSet<ChatMessage> messages, List<ChatMessage> allChatMessages){
		StringBuilder sb = new StringBuilder();
		sb.append("<html><head><style>li { margin: 5px;}</style></head><body><ul>");
		
		for (ChatMessage msg : allChatMessages) {
			if(messages.contains(msg)){
				sb.append("<li>").append(msg.getMessageDate()).append(" : ").append(msg.getMessageContent()).append("</li>");
			}
		}
		
		sb.append("</ul></body></html>");
		
		String fileName = "UnIdentifiedMessages.html";
		String filePath = htmlDirPath + fileName;
		FileUtils.writeToFile(filePath, sb.toString());
		return fileName;
	}
	
	private static String writeSatsanghCenters(String htmlDirPath, List<SatsanghTags> tags, HashMap<SatsanghTags, String> satsangFilePath, String allMessagesPath, String unIdentifiedMessagesPath){
		StringBuilder sb = new StringBuilder();
		int index = 1;
		
		sb.append("<html><head><style>li { margin: 5px;} table, th, tr { border: 1px solid black; }</style></head><body>");
		sb.append("<table><tr><th>SiNo</th><th>Center</th><th>Type</th><th>Day</th></tr>");
		
		for (SatsanghTags tag : tags) {
			
			sb.append("<tr>")
			.append("<td>").append(index).append("</td>")
			.append("<td><a href=\"").append(satsangFilePath.get(tag)).append("\">").append(tag.getCenterName()).append("</a></td>")
			.append("<td>").append(tag.getCenterType()).append("</td>")
			.append("<td>").append(tag.getSatsanghDay()).append("</td>")
			.append("</tr>");
			
			index++;
		}
		
		sb.append("<tr>")
		.append("<td>").append(index).append("</td>")
		.append("<td><a href=\"").append(allMessagesPath).append("\">").append("All Messages").append("</a></td>")
		.append("<td>").append("").append("</td>")
		.append("<td>").append("").append("</td>")
		.append("</tr>");
		
		index++;
		
		sb.append("<tr>")
		.append("<td>").append(index).append("</td>")
		.append("<td><a href=\"").append(unIdentifiedMessagesPath).append("\">").append("UnIdenified Messages").append("</a></td>")
		.append("<td>").append("").append("</td>")
		.append("<td>").append("").append("</td>")
		.append("</tr>");
		
		sb.append("</table></body></html>");
		
		String fileName = "Index.html";
		String filePath = htmlDirPath + fileName;
		FileUtils.writeToFile(filePath, sb.toString());
		return filePath;
	}
}
